/******************************************************************************
 * File:         display.c
 * Version:      1.3
 * Date:         2017-02-11
 * Author:       M. van der Sluys, J.H.L. Onokiewicz, R.B.A. Elsinghorst, J.G. Rouland
 * Description:  OPS exercise 2: syntax check
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "displayFunctions.h"

int main(int argc, char *argv[]) {
  unsigned long int numOfTimes;
  unsigned int niceInc;
  char printMethod, printChar1, printChar2, printChar3;
  ErrCode err;
  
  err = SyntaxCheck(argc, argv);  // Check the command-line parameters
  if(err != NO_ERR) {
      DisplayError(err);        // Print an error message
    } else { 
    printMethod = argv[1][0];
    numOfTimes = strtoul(argv[2], NULL, 10);  // String to unsigned long
    niceInc = atoi(argv[3]);
    printChar1 = argv[4][0];
    printChar2 = argv[5][0];
    printChar3 = argv[6][0];
    
    pid_t child = fork();
    wait(1);
    switch(child){
    case -1: err = ERR_PARS;
      DisplayError(err);
      break;
    case 0: //Process is a child
      nice(niceInc);
      PrintCharacters(printMethod, numOfTimes, printChar2);
      break;
    default: //Process is a parent
      child = fork();
      //Terminate new thread as a child
      break; 
    }

    pid_t child2 = fork();
    wait(1);
    switch(child2){
    case -1: err = ERR_PARS;
      DisplayError(err);
      break;
    case 0: //Process is a child
      nice((niceInc*2));
      PrintCharacters(printMethod, numOfTimes, printChar3);
      break;
    default: //Process is a parent
      child2 = fork();
      //Terminate new thread as a child
      break; 
    }
    
    PrintCharacters(printMethod, numOfTimes, printChar1);
  }
  printf("\n");  // Newline at end
  return 0;
}
