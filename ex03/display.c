/******************************************************************************
 * File:         display.c
 * Version:      1.3
 * Date:         2017-02-11
 * Author:       M. van der Sluys, J.H.L. Onokiewicz, R.B.A. Elsinghorst, J.G. Rouland
 * Description:  OPS exercise 2: syntax check
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "displayFunctions.h"

int main(int argc, char *argv[]) {
  unsigned long int numOfTimes;
  signed int niceValue;
  char printMethod, printChar1, printChar2, printChar3;
  ErrCode err;
  
  err = SyntaxCheck(argc, argv);  // Check the command-line parameters
  if(err != NO_ERR) {
    DisplayError(err);        // Print an error message
  } else {
    printMethod = argv[1][0];
    numOfTimes = strtoul(argv[2], NULL, 10);  // String to unsigned long
    niceValue = strtoul(argv[3], NULL, 2)
    printChar1 = argv[4][0];
    printChar2 = argv[5][0];
    printChar3 = argv[6][0];

    swtich(fork()){
    case -1: err = ERR;
      DisplayError(err);
      break;
    case 0: //Process is a child
      //nice = currentnice + niceValue;
      break;
    case 1: //Process is a parent
      //end fork;
      break;

      
      
    }
    PrintCharacters(printMethod, numOfTimes, printChar);  // Print character printChar numOfTimes times using method printMethod
    
  }
  
  printf("\n");  // Newline at end
  return 0;
}
