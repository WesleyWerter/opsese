/******************************************************************************
 * File:         syntaxCheck.c
 * Version:      1.3
 * Datum:        2017-02-11
 * Author:       M. van der Sluys, J.H.L. Onokiewicz, R.B.A. Elsinghorst, J.G. Rouland
 * Description:  OPS exercise 3:  definitions of test functions for display.c
 ******************************************************************************/

#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "syntaxCheck.h"

// Test whether an argument is one character long and has the correct value (e,p,w):
ErrCode TestType(char *printMethod){
  ErrCode fout = NO_ERR;
  int caseInput;
  
  if(printMethod[0] == 'e'){
    caseInput = 0;
  }else if(printMethod[0] == 'p'){
    caseInput = 1;
  }else if(printMethod[0] == 'w'){
    caseInput = 2;
  }
  
  switch (caseInput){ //Switch case to check if the character is a valid one
  case 0: if(strlen(printMethod) > 1){//If the character is longer than 1, print error
      fout = ERR_TYPE;
    } break;
  case 1: if(strlen(printMethod) > 1){
      fout = ERR_TYPE;
    } break;
  case 2: if(strlen(printMethod) > 1){
      fout = ERR_TYPE;
    } break;
  default: fout = ERR_TYPE;
    break;
  }
  
  return fout;
}


// Test whether an argument contains a non-negative number:
ErrCode TestNr(char *numberOfTimes) {
  ErrCode fout = NO_ERR;
  //int count = 0;
  int count = (atoi(numberOfTimes));
  if(count < 0){
    fout = ERR_NR;
  }
  
  return fout;
}


// Test whether an argument contains only one character:
ErrCode TestChar(char *printChar) {
  ErrCode fout = NO_ERR;
  
  if(strlen(printChar) > 1){
    fout = ERR_CHAR;
  }
  
  return fout;
}
